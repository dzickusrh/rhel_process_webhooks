# SPDX-License-Identifier: GPL-2.0-or-later
"""
Web Hook handlers for Fedora kernel.
"""

import os
from celery import Celery
import gitlab as gitlab_module

app = Celery('tasks', broker=os.environ.get('AMQP_BROKER'))

CONFIG_LABEL = "Configuration"

pool = None

def setup_gitlab():
    URL = os.getenv("GITLAB_URL")
    TOKEN = os.getenv("GITLAB_TOKEN")

    if not URL or not TOKEN:
        raise ("Missing gitlab variables GITLAB_URL or GITLAB_TOKEN")

    return gitlab_module.Gitlab(URL, private_token=TOKEN)


@app.task(queue='fedora')
def merge_request(payload: dict):
    """
    Web hook for when a new merge request is created, an existing merge request
    was updated/merged/closed or a commit is added in the source branch

    The request body is a JSON document documented at
    https://docs.gitlab.com/ce/user/project/integrations/webhooks.html
    """
    gitlab = setup_gitlab()
    project = gitlab.projects.get(payload["project"]["id"])
    merge_request = project.mergerequests.get(payload["object_attributes"]["iid"])

    print("Fedora: Merge Request %s" % merge_request)
    return


    global pool
    if pool is None:
        pool = mp_dummy.Pool()

    _apply_config_label(merge_request)
    pool.apply_async(
        _apply_subsystem_label, (merge_request.project_id, merge_request.iid)
    )


def _drop_ack_nack_labels(payload: dict, merge_request):
    """If the merge request got updated, clear any Acked-by/Nacked-by tags"""
    if payload["object_attributes"]["action"] != "update":
        _log.info("Skipping clearing acks/nacks since %r isn't updated", merge_request)
        return
    if "oldrev" not in payload["object_attributes"]:
        _log.info(
            "Merge request %r got updated, but no code changes were made so "
            "ack/naks are still valid",
            merge_request,
        )
        return

    merge_request.labels = [
        l for l in merge_request.labels if "Acked-by" not in l and "Nacked-by" not in l
    ]
    merge_request.save()


def _apply_subsystem_label(project_id: int, merge_request_id: int):
    """
    Apply a subsystem label to configuration merge requests. This is intended
    to run in a thread as it can take a long time to perform a code search and
    web hooks need to respond quickly. It's possible we'll miss labeling some
    merge requests if the process is killed after acking the web hook and before
    finishing this, but it's not a big deal.

    An entirely new GitLab client is created as the underlying requests session
    is not thread-safe.
    """
    gitlab = gitlab_module.Gitlab(
        settings.FKW_GITLAB_URL, private_token=settings.FKW_GITLAB_TOKEN,
    )
    project = gitlab.projects.get(project_id)
    merge_request = project.mergerequests.get(merge_request_id)
    subsystem_labels = set()
    for change in merge_request.changes()["changes"]:
        file_name = os.path.basename(change["new_path"])
        if file_name.startswith("CONFIG_"):
            results = project.search("blobs", f"config {file_name[7:]}")
            for path in [
                os.path.dirname(p["path"]) for p in results if "Kconfig" in p["path"]
            ]:
                # if drivers/net/ethernet/ do 4 parts at most
                # otherwise do 2 parts at most
                if path.startswith("drivers/net/ethernet/"):
                    subsystem_labels.update(
                        [f"Subsystem: {subsys}" for subsys in path.split("/")[:4]]
                    )
                else:
                    subsystem_labels.update(
                        [f"Subsystem: {subsys}" for subsys in path.split("/")[:2]]
                    )

    if subsystem_labels:
        merge_request.labels += list(subsystem_labels)
        merge_request.save()


def _apply_config_label(merge_request) -> None:
    """Add or remove the CONFIG_LABEL to merge requests."""
    config_dirs = [f"redhat/configs/{flavor}" for flavor in ("fedora", "common", "ark")]
    for change in merge_request.changes()["changes"]:
        for config_dir in config_dirs:
            if change["old_path"].startswith(config_dir) or change[
                "new_path"
            ].startswith(config_dir):
                merge_request.labels.append(CONFIG_LABEL)
                merge_request.save()
                return

    # Nothing touches the configuration so ensure the label is *not* applied.
    if CONFIG_LABEL in merge_request.labels:
        merge_request.labels = [
            label for label in merge_request.labels if label != CONFIG_LABEL
        ]
        merge_request.save()
