"""Pipeline herder matching tasks."""
import os
from celery import Celery

app = Celery('tasks', broker=os.environ['AMQP_BROKER'])


@app.task(queue='sort')
def find_queue(message, event_type):
    """
    Verify message and put into proper queue.
    Queue can go to public worker or internal worker.i
    """

    """Sanity check data"""

    if event_type == 'merge_request':
        fedora.merge_request.delay(message)
        tasks.evaluate_mr.delay(message)
    else:
        log.info("Unsupported event_type: %s" % event_type)
