#!/usr/bin/python3

import requests
import json
import gitlab
import requests
import git
import tempfile
import os
from enum import IntEnum
from celery import Celery
from celery.signals import worker_init

gitdir = None
repo = None

app = Celery('tasks', broker=os.environ['AMQP_BROKER'])


def extract_bzs(commit):
    mlines = commit.message.split('\n')
    for l in mlines:
        if l.startswith('BZ:'):
            l = l[3:]
            l = l.replace(',', ' ')
            return l.split()
    raise Exception('No BZs Found in commit ' + str(commit))

def extract_files(commit):
    filelist = []
    diffstats = commit.stats.files
    for k in diffstats:
        filelist.append(k)
    return filelist

def validate_bzs(mrstate, reviewed_items, user, mr, project):
    for i in reviewed_items:
        last_tgt_release = None
        bzs_valid = []
        i['commit_valid'] = False
        if not i['bzs']:
            i['bzs'] = []
            continue
        i['commit_valid'] = True
        for j in i['bzs']:
            bz_properties = {'bz':j, 'notes': []}
            buginputs = {
                'package': 'kernel',
                'namespace': 'rpms',
                'ref': 'refs/heads/'+mr.target_branch,
                'commits': [{
                    'hexsha': str(i['commit']),
                    'files': i['files'],
                    'resolved': [str(j)],
                    'related': [],
                    'reverted': [],
                }]
            }
            print(json.dumps(buginputs, indent=2))
            # NEED TO FIND A BETTER WAY TO DO THIS - FIXME!
            bugresults = requests.post("https://10.19.208.80/lookaside/gitbz-query.cgi", json=buginputs, verify=False)

            resjson = bugresults.json()
            print(json.dumps(resjson, indent=2))
            if resjson['result'] == 'ok':
                bz_properties['approved'] = True
            else:
                bz_properties['approved'] = False
                errorstring = resjson['error'].replace('\n', ' ')
                bz_properties['notes'].append(errorstring)
            if mrstate.flags['bzreportlogs'] == True:
                bz_properties['logs'] = resjson['logs']

            bzs_valid.append(bz_properties)
        i['bzs'] = bzs_valid

def print_bz_report(mrstate, project, mr, reviewed_items):
    mr_approved = True
    report = ""
    table = []
    for i in reviewed_items:
        if i['commit_valid'] == False:
            print("Commit is not valid")
            mr_approved = False
        if mr_approved == True:
            table.append([i['commit'], "", "", ""])
        else:
            table.append([i['commit'], "", "", "Commit contains no valid bzs, please add a line to the changelog\nin the format BZ: <bugzilla>[,<bugzilla>...]"])
        for j in i['bzs']:
            if j['approved'] == False:
                mr_approved = False
            notestring = ""
            for n in j['notes']:
                notestring = notestring + n + "<br>"
            table.append(["", j['bz'], j['approved'], notestring])
    report = "Project: " + project.name_with_namespace + "   \n"
    report += "Project ID: " + str(project.id) + "   \n"
    report += "Merge Request ID: " + str(mr.iid) + "   \n"
    report += "Target Branch: " + mr.target_branch + "   \n"
    report += "Note that acceptance rules are based on target branch and   \n"
    report += "are established by Red Hat policy.  If a given bz fails   \n"
    report += "validation, please submit a query in that bugzilla   \n"
    report += " \n"
    report += "BZ READINESS REPORT:\n\n"
    report += "|Commit|BZ|Approved|Notes|\n"
    report += "|:------|:------|:------|:-------|\n"
    for r in table:
        report += "|"+str(r[0])+"|"+r[1]+"|"+str(r[2])+"|"+r[3]+"|\n"
    
    report += "\n\n"
    if mr_approved == True:
        report += "Merge Request passes bz validation\n"
    else:
        report += "\nMerge Request fails bz validation.  \n"
        report += " \n"
        report += "To request re-evalution after getting bz approval "
        report += "add a comment to this MR with only the text: "
        report += "request-bz-evaluation "

    if mrstate.flags['bzreportlogs'] == True:
        report += "LOGS:   \n"
        for i in reviewed_items:
            for j in i['bzs']:
                report += j['logs'] + "   \n"
    return (report, mr_approved)

def check_on_bzs(mrstate, lab, mr, project):
    review_lists = []
    repo.remotes['origin'].fetch("merge-requests/"+str(mr.iid)+"/head:"+str(mr.iid))
    for c in mr.commits():
        commit = repo.commit(c.id)
        try:
            found_bzs = extract_bzs(commit)    
        except:
            found_bzs = []
        try:
            found_files = extract_files(commit)
        except:
            found_files = []

        review_lists.append({'commit': commit, 'bzs': found_bzs, 'files': found_files})
    try:
        user = lab.users.get(mr.author['id'])
    except:
        user = None
    validate_bzs(mrstate, review_lists, user, mr, project)
    try:
        repo.git.branch("-D", str(mr.iid))
    except Exception as e:
        print("Odd....Error in deleting head: " + str(e))
    return print_bz_report(mrstate, project, mr, review_lists)

class State(IntEnum):
        NEW = 0
        BZVALID = 1 
        CIVALID = 2 
        CIBZVALID = 3 
        READYTOMERGE = 4 
        MAINTOVERRIDE = 5 
        UNKNOWN = 6 

class MRState():
    def __init__(self, lab, project, mr, payload):
        self.lab = lab
        self.project = project
        self.mr = mr
        self.payload = payload
        self.need_update = False
        # Set our initial state based on our bz and ci labels
        self.currentState = self.compute_label_state()
        self.nextState = State.UNKNOWN
        self.flags = {'bzreportlogs': False}

    def compute_label_state(self):
        current_state = State.NEW
        dont_care_labels = ['maintainerOverride', 'readyForMerge',
                            'bzValidationFails']
        labels_to_states = [
            {'label': set([]), 'state': State.NEW},
            {'label': set(['bzValidated']), 'state': State.BZVALID},
            {'label': set(['passesCI']), 'state': State.CIVALID},
            {'label': set(['bzValidated', 'passesCI']), 'state': State.CIBZVALID}
        ]
        # make a copy of our labels
        valid_labels = self.mr.labels.copy()
        # remoe the labels that aren't relevant right now
        for i in dont_care_labels:
            try:
                valid_labels.remove(i)
            except:
                pass

        # Find the initial state we are in
        for i in labels_to_states:
            if set(valid_labels) == i['label']:
                current_state = i['state']
                break

        # Now we need to make some adjustments based on maintainerOverride and ReadyForMerge
        # If the maintainer override flag has been applied, then we are in MAINTOVERRIDE state
        if 'maintainerOverride' in self.mr.labels:
            current_state = State.MAINTOVERRIDE
        # If the readyForMerge flag is set, then we are in ReadyToMerge state
        if 'readyForMerge' in self.mr.labels:
            current_state = State.READYTOMERGE

        return current_state

    def add_label(self, label):
        if label not in self.mr.labels:
            self.mr.labels.append(label)
            self.need_update = True

    def remove_label(self, label):
        if label in self.mr.labels:
            self.mr.labels.remove(label)
            self.need_update = True

    def add_if_not_label(self, label, iflabel):
        if iflabel not in self.mr.labels:
            if label not in self.mr.labels:
                self.mr.labels.append(label)
                self.need_update = True
           
    def remove_if_not_label(self, label, iflabel):
        if iflabel not in self.mr.labels:
            if label in self.mr.labels:
                self.mr.labels.remove(label)
                self.need_update = True

    def update_mr_state(self):
        if self.nextState == State.NEW:
            self.remove_label('bzValidated')
            self.remove_label('passesCI')
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
        elif self.nextState == State.BZVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.remove_label('passesCI')
            self.add_label('bzValidated')
        elif self.nextState == State.CIVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.remove_label('bzValidated')
            self.add_label('passesCI')
        elif self.nextState == State.CIBZVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.add_label('passesCI')
            self.add_label('bzValidated')
        elif self.nextState == State.READYTOMERGE:
            self.add_label('readyForMerge')
        elif self.nextState == State.MAINTOVERRIDE:
            self.add_label('maintainerOverride')

        if self.need_update == True:
            self.mr.save()

    def force_mr_state(self, state):
        self.currentState = state
        print("Forcing MR state back to " + str(self.currentState), flush=True)

    def update_current_state(self):
        self.currentState = self.nextState
        print("Updating MR current state to " + str(self.currentState), flush=True)

    def compute_next_state(self):
        print("Computing next state from state " + str(self.currentState), flush=True)
        if self.currentState == State.NEW:
            self.__run_bz_validation()
            # need to move the state machine along so that the ci state is correct
            self.update_current_state()
            self.__check_ci_result()
        elif self.currentState == State.BZVALID:
            self.__check_ci_result()
        elif self.currentState == State.CIVALID:
            self.__run_bz_validation()
        elif self.currentState == State.CIBZVALID:
            self.__check_approvals()
        elif self.currentState == State.READYTOMERGE:
            self.__check_approvals()
        elif self.currentState == State.MAINTOVERRIDE:
            self.__force_ready_to_merge()
        else:
            self.__log_unknown_state()

        print("Computed next state as " + str(self.nextState), flush=True)
        return

    def __check_ci_result(self):
        print("Checking CI state\n", flush=True)
        self.nextState = self.currentState
        if self.payload['object_kind'] == 'pipeline':
            # We're running this because a pipeline completed 
            print("pipeline state is " + self.payload['object_attributes']['status'])
            if self.payload['object_attributes']['status'] == 'success':
                if self.currentState == State.BZVALID:
                    self.nextState = State.CIBZVALID
                else:
                    self.nextState = State.CIVALID
        else:
            print("Cehcking MR pipeline result\n", flush=True)
            # We're running because we got an MR update
            if self.mr.pipeline != None:
                if self.mr.pipeline['status'] == 'success':
                    if self.currentState == State.BZVALID:
                        self.nextState = State.CIBZVALID
                    else:
                        self.nextState = State.CIVALID
            else:
                print("Checking CI as MR with no pipeline!\n", flush=True)
                # No relevant pipeline status, keep our current state
                self.nextState = self.currentState

        if self.nextState == State.CIBZVALID:
            self.update_current_state()
            self.__check_approvals()

    def __run_bz_validation(self):
        self.nextState = self.currentState 
        print("Running bz validation\n", flush=True)
        if 'bzValidationFails' in self.mr.labels:
            return
        (report, approved) = check_on_bzs(self, self.lab, self.mr, self.project)
        self.mr.notes.create({'body': report})
        if (approved == True):
            if self.currentState == State.CIVALID:
                self.nextState = State.CIBZVALID
            else:
                self.nextState = State.BZVALID
        else:
            print("WE FAILED BZ VALIDATION!  MARK US AS SUCH!\n")
            # This is not really part of the state machine, but rather a marker,
            # to prevent us from having to create several more states to
            # consider
            self.mr.labels.append('bzValidationFails')
            self.need_update = True
        if self.nextState == State.CIBZVALID:
            self_update_current_state()
            self.__check_approvals()

    def __check_approvals(self):
        approvals = self.mr.approvals.get()
        print("We have " + str(len(approvals.approved_by)) + " approvals")
        if len(approvals.approved_by) >= 1:
            if self.currentState == State.CIBZVALID:
                # This MR is ready for merging
                print("Marking MR as ready")
                self.nextState = State.READYTOMERGE
            elif self.currentSTate == State.READYTOMERGE:
                self.nextState = State.READYTOMERGE
        else:
            if self.currentState == State.READYTOMERGE:
                if 'bzValidated' in self.mr.labels:
                    self.nextState == State.BZVALID
                    if 'passesCI' in self.mr.labels:
                        self.nextState = State.CIBZVALID
                elif 'passesCI' in self.mr.labels:
                        self.nextState = State.CIVALID
            else:
                self.nextState = self.currentState

    def __do_nothing(self):
        self.nextState = self.currentState
        return

    def __force_ready_to_merge(self):
        self.nextState = State.READYTOMERGE
        
    def __log_unknown_state(self):
        print("MR " + str(mr.iid) + " IS IN UNKNOWN STATE")

@worker_init.connect
def init_worker(sender=None, headers=None, body=None, **kwargs):
    global repo
    global gitdir
    gitdir = tempfile.TemporaryDirectory()
    repo = git.Repo.clone_from(os.environ['GIT_TREE'], to_path=gitdir.name)
    print("Setup git repo in " + gitdir.name)

@app.task(queue='internal')
def evaluate_mr(payload):
    lab = gitlab.Gitlab('https://gitlab.com', private_token=os.environ['GITLAB_API_KEY'], ssl_verify=False)

    print("Internal: object_kind %s"% payload['object_kind'])
    return


    mr = None
    project = None
    mrstate = None
    if payload['object_kind'] == 'pipeline':
        print("Checking pipeline request\n")
        # its a pipeline event, apply the passesCI label if the pipeline
        # is successful
        project = lab.projects.get(payload['project']['id'])
        # We don't care about this event if there isn't an associated merge
        # request
        if payload['merge_request'] == None:
            return
        mr = project.mergerequests.get(payload['merge_request']['iid'])
        mrstate = MRState(lab, project, mr, payload)
        # Force our state back to a state that requires CI checking
        if mrstate.currentState == State.CIVALID:
            mrstate.force_mr_state(State.NEW)
        elif mrstate.currentState == State.CIBZVALID:
            mrstate.force_mr_state(State.BZVALID)

    elif payload['object_kind'] == 'note':
        print("Checking note request\m")
        notetext = payload['object_attributes']['note']
        project = lab.projects.get(payload['project']['id'])
        mr = project.mergerequests.get(payload['merge_request']['iid'])
        mrstate = MRState(lab, project, mr, payload)
 
        if notetext.startswith("request-bz-evaluation"):
            if notetext == "request-bz-evaluation(logs)":
                print("Bz report requests full logs")
                mrstate.flags['bzreportlogs'] = True
            elif notetext != "request-bz-evaluation":
                # Do nothing if the text is not either of the above 
                # 2 variants
                return

            # Force a re-run of the bz validation
            try:
                print("Removing bzValidationFails\n", flush=True)
                mr.labels.remove('bzValidationFails')
            except:
                pass
            mrstate.force_mr_state(State.NEW)
        elif notetext == "request-pipeline-evaluation":
            mrstate.force_mr_state(State.BZVALID)
        else:
            #This is a note we can ignore, just return here
            return

    elif payload['object_kind'] == 'merge_request':
        project = lab.projects.get(payload['project']['id'])
        mr = project.mergerequests.get(payload['object_attributes']['iid'])
        mrstate = MRState(lab, project, mr, payload)
    else:
        print("Doing nothing with object kind " + payload['object_kind'])
        return

    if mrstate != None:
        mrstate.compute_next_state()
        mrstate.update_mr_state()    

