"""Pipeline herder worker."""
import os

#import sentry_sdk
from celery import Celery
#from sentry_sdk.integrations.celery import CeleryIntegration

if os.environ.get('FLASK_ENV', 'production') == 'production':
    # the DSN is read from SENTRY_DSN
#    sentry_sdk.init(
#        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
#        integrations=[CeleryIntegration()]
#    )
    pass

app = Celery('tasks', broker=os.environ.get('AMQP_BROKER'))
app.autodiscover_tasks(['webhooks.tasks', 'webhooks.fedora'],
                       related_name=None)
