setup:

* podman build -f Dockerfile.flask .
* podman tag <imageid> gitlab-webhook
* podman build -f Dockerfile.celery .
* podman tag <imageid> gitlab-celery-worker
* podman pod create --network=host
* podman run --pod <podid> rabbitmq
* podman run --pod <podid> --env BZ_API_KEY=<key> --env GITLAB_API_KEY=<key> --env AMQP_BROKER=<uri> --env GIT_TREE=<git tree> gitlab-celery-worker
* podman run --pod <podid> --env AMQP_BROKER=<uri> gitlab-webhook

