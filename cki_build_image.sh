#!/bin/bash
set -euo pipefail

# Make a note of the package versions.
buildah --version

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

function say {
    echo "$@" | toilet -f mono12 -w 400 | lolcat -f || true
}

# Build the container.
say "build"

# Determine whether/how to push the image:
# - if a tag was provided, use it for the image
# - if this is an MR pipeline, use mr-1234
# - if we're on the master branch, use 'latest'
# - otherwise, do not push
if [ -v CI_COMMIT_TAG ]; then
    IMAGE_TAG=${CI_COMMIT_TAG}
    IMAGE_DESCRIPTION=${CI_PROJECT_PATH}/${IMAGE_NAME}:${IMAGE_TAG}
elif [ -v CI_MERGE_REQUEST_IID ]; then
    IMAGE_TAG=mr-${CI_MERGE_REQUEST_IID}
    IMAGE_DESCRIPTION=${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH}:${IMAGE_TAG}
elif [ "${CI_COMMIT_REF_NAME:-}" = 'master' ]; then
    IMAGE_TAG="latest"
    IMAGE_DESCRIPTION=${CI_PROJECT_PATH}/${IMAGE_NAME}:${IMAGE_TAG}
fi

function build_image {
    CPATH=includes:/usr/local/share/cki buildah bud \
        ${base_image+--build-arg BASE_IMAGE=$base_image} \
        ${base_image_tag+--build-arg BASE_IMAGE_TAG=$base_image_tag} \
        ${IMAGE_DESCRIPTION+--build-arg IMAGE_DESCRIPTION=$IMAGE_DESCRIPTION} \
        -f "builds/${IMAGE_NAME}"* \
        -t "${IMAGE_NAME}" \
        .
}
# try to cope with networking and registry issues
for i in {1..5}; do build_image && s=0 && break || s=$? && sleep 10; done; (exit $s)

if ! [ -v IMAGE_TAG ]; then
    exit 0
fi

if [ -n "${CI_REGISTRY:-}" ]; then
    # Prepare to push to GitLab's container registry.
    say "gitlab push"
    export REGISTRY_AUTH_FILE=${HOME}/auth.json
    echo "${CI_REGISTRY_PASSWORD}" | buildah login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
    GITLAB_TAG=${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${IMAGE_TAG}
    buildah tag "${IMAGE_NAME}" "${GITLAB_TAG}"
    echo "Publishing to $GITLAB_TAG"

    # Push to registry.gitlab.com.
    for i in {1..5}; do
        echo "Attempt #${i} to push to registry.gitlab.com..."
        if buildah push "${GITLAB_TAG}"; then
            PUSH_GITLAB_OK=1
            break
        fi
    done
else
    PUSH_GITLAB_OK=1
fi

# If any push failed, fail the pipeline.
if [[ -z ${PUSH_GITLAB_OK:-} ]]; then
    echo "The container push failed to one or more destinations."
    exit 1
fi
